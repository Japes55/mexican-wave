﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpectatorScript : MonoBehaviour
{
    public Material PlayerControlledMaterial;
    private Material _defaultMaterial;
    private Color _defaultColor;
    bool _playerAffectingWoo = false;


    Vector3 _scale;
    Vector3 _pos;

    float _wooStartTime = -100f;
    float _wooLength = 0.75f;
    bool _skippingWoo = false;

    public bool IsControlled = false;

    BezierFollow _waveMarker;
    GlobalLogicScript _globalLogic;
    PCGenerator _pcGenerator;

    bool _wasInWavePreviously = false;


    // Start is called before the first frame update
    void Start()
    {
        SetColors();
        _scale = transform.localScale;
        _pos = transform.position;
        _waveMarker = GameObject.Find("WaveMarker").GetComponent<BezierFollow>();
        _globalLogic = GameObject.Find("GlobalLogic").GetComponent<GlobalLogicScript>();
        _pcGenerator = GameObject.Find("GlobalLogic").GetComponent<PCGenerator>();

        _wooLength = Random.Range(0.45f, 1.25f);
    }

    void SetColors()
    {
        if(IsControlled) {
            GetComponent<Renderer>().material = PlayerControlledMaterial;
        }
        _defaultMaterial = GetComponent<Renderer>().material;
        _defaultColor = _defaultMaterial.color;
    }

    // Update is called once per frame
    void Update()
    {
        if(!IsControlled) {
            UpdateNPC();
        } else {
            UpdatePC();
        }

        _wasInWavePreviously = InWave();
    }

    void UpdatePC()
    {
        _wooLength = 0.8f;
        if(!IsWooing()) {
            GetComponent<Renderer>().material.color = _defaultColor;
        }

        if(Input.GetKeyDown(KeyCode.Space) && !IsWooing()) {
            Woo(); //woo!
            SetWooState(true);

            if(!InWave()) {
                return;
            }

            double adjustmentPerc = (20d - DistanceFromWave()) / 20d;
            if(adjustmentPerc < 0) {
                return;
            }

            GetComponent<Renderer>().material.color = Color.Lerp(_defaultColor, Color.red, (float)(adjustmentPerc*adjustmentPerc));;
            _globalLogic.BoostWave(adjustmentPerc);
        
        }

        SetWooState(IsWooing());

        if(!_wasInWavePreviously && InWave()) {
            _pcGenerator.RegisterPC();
        }
    }

    void UpdateNPC()
    {
        if(InWave() && !_wasInWavePreviously) {  

            if(_pcGenerator.CanMakePC) {
                IsControlled = true;
                SetColors();

                _pcGenerator.RegisterPC();
                _pcGenerator.CanMakePC = false;
                
                return;
            }

            Woo(); //woo!
        }

        SetWooState(IsWooing() && !_skippingWoo);
    }

    //start WOOOOing, i.e. standing up for the mexican wave
    void Woo()
    {
        _wooStartTime = Time.time;

        _skippingWoo = false;
        if(Random.Range(0.0f, 1.0f) < _globalLogic.GetSkipChance()) {
            _skippingWoo = true;
        }
    }

    void SetWooState(bool wooing)
    {
        if(wooing) {
            gameObject.transform.localScale = new Vector3(_scale.x*1.2f, _scale.y*2, _scale.z);
            GetComponent<Renderer>().material.color = Color.Lerp(_defaultColor, Color.red, (float)(_globalLogic._wavePower/3f));
        } else {
            GetComponent<Renderer>().material.color = _defaultColor;
            gameObject.transform.localScale = _scale;
        }
        transform.position = new Vector3(_pos.x, _pos.y + (gameObject.transform.localScale.y - _scale.y)/2, _pos.z);
    }

    bool IsWooing() {
        var t = Time.time;
        return (t >= _wooStartTime && (t - _wooStartTime) < _wooLength);
    }

    bool InWave()
    {
        Vector3 yPlanePos = new Vector3(transform.position.x, 0, transform.position.z);
        Vector3 vecToMe = yPlanePos - _waveMarker.GetCurrentPosition();
        Vector3 waveNormal = _waveMarker.GetCurrentNormal();

        float angle = Vector3.SignedAngle(waveNormal, vecToMe, Vector3.up);

        if(angle > 0 && angle < 90f) {
            // wave is in front of us
            return true;
        }

        return false;
    }
    float DistanceFromWave()
    {
        //we want to get the distance between:
        // - our position projected onto the ground
        // - the normal vector going through the bezier curve at the point the wave is currently
        //turns out you can get the the distance as follows:
        // - make another line going from one end of the line to your point
        // - get the cross product of these 2 lines
        // - get the magnitude, divided by the magnitude of the line.

        Vector3 yPlanePos = new Vector3(transform.position.x, 0, transform.position.z);
        Vector3 vecToMe = yPlanePos - _waveMarker.GetCurrentPosition();
        Vector3 waveNormal = _waveMarker.GetCurrentNormal();

        var crossProd = Vector3.Cross(waveNormal, vecToMe);
        if (Vector3.Angle(waveNormal, vecToMe) < 90) {
            return crossProd.magnitude / waveNormal.magnitude;
        } else {
            //(don't care if we are on the opposite side of the stadium)
            return 100000000000;
        }
    }
}
