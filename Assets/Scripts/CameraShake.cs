﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
	// Transform of the camera to shake. Grabs the gameObject's transform
	// if null.
	public Transform camTransform;
	
	// How long the object should shake for.
	float shakeDuration = 0f;
	
	// Amplitude of the shake. A larger value shakes the camera harder.
	public float shakeAmount = 0.7f;
	public float decreaseFactor = 1.0f;
	
	float _shakeLvl = 0;

	Vector3 originalPos;
	
	void Awake()
	{
	}
	
	void Start()
	{
		if (camTransform == null)
		{
            camTransform = GameObject.Find("Main Camera").transform;
	    	originalPos = camTransform.position;
		}
	}

    public void SetShakeLevel(float lvl)
	{
		//set a continuous shake, level of shake will be lvl*shakeAmount
		_shakeLvl = lvl;
	}

    public void OneShake(float duration)
    {
        shakeDuration = duration;
    }

	void Update()
	{
		if(_shakeLvl > 0) {
			camTransform.position = originalPos + Random.insideUnitSphere * shakeAmount * _shakeLvl;
			return;
		}

		if (shakeDuration > 0) {
			camTransform.position = originalPos + Random.insideUnitSphere * shakeAmount * shakeDuration;
			shakeDuration -= Time.deltaTime * decreaseFactor;
		} else if(shakeDuration < 0.0f) {
			shakeDuration = 0f;
    	    camTransform.position = originalPos;
		}
	}
}