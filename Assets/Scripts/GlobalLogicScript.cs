﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GlobalLogicScript : MonoBehaviour
{
    float _skipChanceincreaseRateRate;
    float _skipChanceincreaseRate;
    public double _wavePower;
    float _waveDecayRate;
    float _startingWaveMarkerSpeed;

    float _numPCMultiplier;
    float _lastPCGenPos;

    BezierFollow _waveMarker;

    PCGenerator _pcGenerator;

    GameObject _gameOverPanel;
    Text _gameOverText;
    GameObject _speechPanel;
    Text _speechText;

    float _gameStartTime;
    float _gameStartProgress;

    float _errorMargin;
    float _previousPos;

    bool _haveStartedPlaying;

    Slider _slider;

    AudioSource _waveAudioSource;

    List<PyroControl> _pyros;

    // Start is called before the first frame update
    void Start()
    {
        _waveMarker = GameObject.Find("WaveMarker").GetComponent<BezierFollow>();
        _waveAudioSource = GameObject.Find("WaveMarker").GetComponent<AudioSource>();
        _pcGenerator = gameObject.GetComponent<PCGenerator>();
        _gameOverPanel = GameObject.Find("GameOverPanel");
        _gameOverText = _gameOverPanel.transform.Find("BodyText").GetComponent<Text>();
        _speechPanel = GameObject.Find("SpeechPanel");
        _speechText = GameObject.Find("SpeechText").GetComponent<Text>();
        _slider = GameObject.Find("Slider").GetComponent<Slider>();

        _pyros = new List<PyroControl>();
        var pyros = GameObject.FindGameObjectsWithTag("Pyro");
        foreach(var pyro in pyros) {
            _pyros.Add(pyro.GetComponent<PyroControl>());
        }

        _startingWaveMarkerSpeed = _waveMarker.Speed;
        _skipChanceincreaseRateRate = 0f;
        _skipChanceincreaseRate = 0f;
        _wavePower = 0.9f;
        _speechText.text = "";
        _gameStartTime = Time.time;
        _gameStartProgress = 0;
        _errorMargin = 0.2f;
        _haveStartedPlaying = false;
        _waveDecayRate = 0.75f;
        _waveMarker.Speed = _startingWaveMarkerSpeed;
        _numPCMultiplier = 1f;

        _gameOverPanel.SetActive(false);
        _slider.gameObject.SetActive(false);

        InvokeRepeating("GeneratePC", 25, 25);
    }

    // Update is called once per frame
    void Update()
    {
        float currentPos = _waveMarker.GetCurrentProgress();

        SetWaveVolume(currentPos);

        if(_haveStartedPlaying) {
            /*
            float posOfNextPC = _pcGenerator.PosOfNextPC(currentPos);
            float posOfLastPC =  _pcGenerator.PosOfLastPC(currentPos);
            if(posOfNextPC <= posOfLastPC) {
                posOfNextPC += 1;
            }
            float distBetween = posOfNextPC - posOfLastPC;
            float decreaseRate = (1 - _errorMargin) / distBetween;

            AdjustWavePower(-decreaseRate * (_waveMarker.ProgressLastFrame()));

            float marginDecreaseRate = 0.04f/9f;
            _errorMargin -= marginDecreaseRate*Time.deltaTime;
            //Debug.Log("_errorMargin: " + _errorMargin);
            */

            //AdjustWavePower(-_waveDecayRate * Time.deltaTime );

            if(currentPos <= _previousPos) {
                _numPCMultiplier = _pcGenerator.NumPCs();
            }
            AdjustWavePower(-_waveDecayRate * _numPCMultiplier * _waveMarker.ProgressLastFrame() );
            
            _waveMarker.Speed += 0.005f * Time.deltaTime;
            
        }

        _slider.value = (float)(_wavePower);

        if(Input.GetKeyDown(KeyCode.P)) {
            _pcGenerator.GeneratePC();
        }
        if(Input.GetKeyDown(KeyCode.Q)) {
            ShootPyro();
        }


        if(_wavePower <= 0 && !_gameOverPanel.activeSelf) {
            _gameOverPanel.SetActive(true);
            _gameOverText.text = "Your mexican wave did a total of " + (_waveMarker.GetTotalProgress() - _gameStartProgress).ToString("F2") + " loops :)";
        }

        _skipChanceincreaseRate += Time.deltaTime * _skipChanceincreaseRateRate;
        _previousPos = currentPos;
        HandleTutorial();
    }

    void GeneratePC()
    {
        if( _pcGenerator.NumPCs() < 4) {
            _pcGenerator.GeneratePC();
        }
    }

    void SetWaveVolume(float currentPos)
    {
        float closestProgressPoint = 0.625f; //hardcoded to be the pos closest to the camera
        float audioDist = Mathf.Abs(closestProgressPoint - currentPos);
        if(audioDist > 0.5f) {
            audioDist = 1.0f - audioDist;
        }

        audioDist *= 2;

        _waveAudioSource.volume = (1 - audioDist)*(1 - audioDist);
        gameObject.GetComponent<CameraShake>().SetShakeLevel((1 - audioDist)*(1 - audioDist)*(1 - audioDist)*(1 - audioDist));
    }

    void HandleTutorial()
    {
        float timeElapsed = Time.time -_gameStartTime;
        if(timeElapsed > 2.5) {
            _speechText.text = "Hey check it out! A mexican wave!";
        } 
        if (timeElapsed > 5.5) {
            _speechText.text = "...we should try to keep it alive :)";
        }
        if (timeElapsed > 8.5) {
            _speechText.text = "press SPACEBAR to cheer";
        }
        if (timeElapsed > 11) {
            _speechText.text = "cheer as the wave passes to help keep it going!";
        }
        if (timeElapsed > 14) {
            _speechText.text = "Cheer as close as possible to the START of the wave...";
        }
        if (timeElapsed > 17) {
            _speechText.text = "...but not BEFORE it arrives, or you'll have no effect!";
        }
        if (timeElapsed > 20) {
            _speechText.text = "(the darker the wave, the stronger it is)";
        }
        if (timeElapsed > 24) {
            _speechText.text = "Last thing: others may want to help you cheer on the wave...";
        }
        if (timeElapsed > 27) {
            _speechText.text = "...if they appear, use them to cheer too!";
        }
        if (timeElapsed > 30) {
            _speechText.text = "Good luck!";
        }
        if (timeElapsed > 31) {
            _speechPanel.SetActive(false);
        }
    }

    public void ResetGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name,  LoadSceneMode.Single);
    }

    public void BoostWave(double powerPerc) {

        if(_gameOverPanel.activeSelf) {
            return;
        }

        if(powerPerc > 0.95) {
            ShootPyro();
        }

        //TODO:
        //we want things never to be impossible, but to get progressively harder
        //with zero margin for error, wave decays just after reaching next dude
        //_pcGenerator.NumPCs(); ...

        //first time player does this, it's on!
        if(_gameStartProgress == 0) {
            _skipChanceincreaseRateRate = 0.005f;
            _skipChanceincreaseRate = 0.05f;
            _gameStartProgress = _waveMarker.GetTotalProgress();
            _haveStartedPlaying = true;
        }

        float powerFactor = 1f;
        double adjustment = powerPerc*powerPerc*powerPerc*powerPerc*powerFactor;
        Debug.Log("powerPerc: " + powerPerc + " adjustment: " + adjustment);
        AdjustWavePower(adjustment);
    }

    void ShootPyro()
    {
        foreach(var pyro in _pyros) {
            pyro.Shoot();
        }
    }

    public void AdjustWavePower(double amount) {
        _wavePower += amount;

/*
        if(_wavePower > 1) {
            _wavePower = 1;
        }

        if(_wavePower < 0) {
            _wavePower = 0;
        }
*/
        if(amount >= 0) {
            //Debug.Log("boosted to " + _wavePower);
        }
    }

    public double GetSkipChance() {
        return (1d - _wavePower) - 0.2f;
    }
}
