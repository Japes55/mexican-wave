﻿using UnityEngine;

//TO BE USED WITH BEZIERFOLLOWER
//VISUALIZATION IS AWESOME, SPACING OF SPHERES REPRESENTS RELATIVE SPEED THAT FOLLOWER WILL MOVE AT
//FIDDLE WITH POINTS AND WITH SPEEDFACTOR TO GET A NICE SMOOTH PATH

public class BezierVisualiser : MonoBehaviour
{
    [SerializeField]
    private Transform[] controlPoints;

    public float speedFactor = 1f;

    private Vector3 _p0;
    private Vector3 _p1;
    private Vector3 _p2;
    private Vector3 _p3;

    private void OnDrawGizmos()
    {
        var pos0 = controlPoints[0].position;
        var pos1 = controlPoints[1].position;
        var pos2 = controlPoints[2].position;
        var pos3 = controlPoints[3].position;

        Gizmos.color = Color.magenta;
        float step = 0.05f;
        for(float t = 0; t <= 1; t += step*speedFactor)
        {
            var bezierPos = GetBezierPos(t, pos0, pos1, pos2, pos3);
            Gizmos.DrawSphere(bezierPos, 0.25F);

            var tangent = GetBezierTangent(t, pos0, pos1, pos2, pos3);
            var tanPos1 = bezierPos - tangent * step*3;
            var tanPos2 = bezierPos + tangent * step*3;
            Gizmos.DrawLine(tanPos1, tanPos2);

            var normalSlopeOnYPlane = new Vector3(tangent.z, 0, -tangent.x);
            var normPos1 = bezierPos - normalSlopeOnYPlane * step*3;
            var normPos2 = bezierPos + normalSlopeOnYPlane * step*3;
            Gizmos.DrawLine(normPos1, normPos2);
        }

        Gizmos.DrawLine(pos0, pos1);
        Gizmos.DrawLine(pos2, pos3);
    }

    void Start()
    {
        _p0 = controlPoints[0].position;
        _p1 = controlPoints[1].position;
        _p2 = controlPoints[2].position;
        _p3 = controlPoints[3].position;
    }

    private Vector3 GetBezierPos(float t, Vector3 pos0, Vector3 pos1, Vector3 pos2, Vector3 pos3)
    {
        return Mathf.Pow(1 - t, 3) * pos0 +
                3 * Mathf.Pow(1 - t, 2) * t * pos1 + 
                3 * (1-t) * Mathf.Pow(t,2) * pos2 + 
                Mathf.Pow(t,3) * pos3;
    }

    private Vector3 GetBezierTangent(float t, Vector3 pos0, Vector3 pos1, Vector3 pos2, Vector3 pos3)
    {
        return 3 * Mathf.Pow(1 - t, 2) * (pos1-pos0) + 6*(1-t)*t*(pos2 - pos1) + 3 * Mathf.Pow(t, 2) * (pos3 - pos2);
    }

    public Vector3 GetBezierPos(float t)
    {
        return GetBezierPos(t, _p0, _p1, _p2, _p3);
    }

    private Vector3 GetNormalOnY(Vector3 curve)
    {
        //NOTE this gets the normal on the y-plane specifically.
        //there are actually infinitely many normals to a 3d curve at any point.
        return new Vector3(curve.z, 0, -curve.x);
    }

    public Vector3 GetBezierNormal(float t)
    {
        var tangent = GetBezierTangent(t,  _p0, _p1, _p2, _p3);
        return GetNormalOnY(tangent);
    }
}
