﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWaveBall : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Ray mouseToWorldRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        //Shoots a ray into the 3D world starting at our mouseposition
        if (Physics.Raycast(mouseToWorldRay, out hitInfo))
        {
            transform.position = new Vector3(hitInfo.point.x, transform.position.y, transform.position.z);
        }

    }
}
