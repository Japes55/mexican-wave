﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierFollow : MonoBehaviour
{

    [SerializeField]
    private GameObject[] routes;

    private int _currentRouteNum;
    private float _t;
    public float Speed;

    private int _numLoops;

    private Vector3 _currentNormal;

    private float _lastProgress;
    private float _progressLastFrame;

    // Start is called before the first frame update
    void Start()
    {
        Speed = 0.5f;
        Reset();
    }

    // Update is called once per frame
    void Update()
    {
        var route = routes[_currentRouteNum];
        BezierVisualiser viz = route.GetComponent<BezierVisualiser>();

        _t += Time.deltaTime * Speed * viz.speedFactor;
        transform.position = viz.GetBezierPos(_t);
        _currentNormal = viz.GetBezierNormal(_t);

        if(_t >= 1) {
            _t = 0;
            _currentRouteNum += 1;
            if(_currentRouteNum > routes.Length - 1) {
                _numLoops += 1;
                _currentRouteNum = 0;
            }
        }

        _progressLastFrame = GetTotalProgress() - _lastProgress;
        _lastProgress = GetTotalProgress();
    }

    public void Reset()
    {
        _t = 0;
        _currentRouteNum = 0;
        _numLoops = 0;
    }

    public Vector3 GetCurrentNormal()
    {
        return _currentNormal;
    }

    public Vector3 GetCurrentPosition()
    {
        return transform.position;
    }

    public float GetCurrentProgress()
    {
        return (_currentRouteNum + _t) / routes.Length;
    }

    public float GetTotalProgress()
    {
        return _numLoops + GetCurrentProgress();
    }

    public float ProgressLastFrame()
    {
        return _progressLastFrame;
    }


}
