﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PyroControl : MonoBehaviour
{
    ParticleSystem _particles;
    AudioSource _audioSource;
    CameraShake _cameraShake;

    // Start is called before the first frame update
    void Start()
    {
        _particles = gameObject.GetComponentInChildren<ParticleSystem>();
        _audioSource = gameObject.GetComponent<AudioSource>();
        _cameraShake = gameObject.GetComponent<CameraShake>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Shoot()
    {
        _particles.Emit(300);
        _audioSource.Play();
        _cameraShake.OneShake(0.5f); //TODO this doesn't work because the globalcontrol camera shake resets the camera position every frame...
    }
}
