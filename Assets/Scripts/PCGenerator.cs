﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PCGenerator : MonoBehaviour
{
    BezierFollow _waveMarker;

    public bool CanMakePC = false;
    private List<float> _pcPositions;
    private float _nextPCposition = 2;
    float _previousMarkerPos;

    private bool _willGeneratePC = false;

    // Start is called before the first frame update
    void Start()
    {
        _waveMarker = GameObject.Find("WaveMarker").GetComponent<BezierFollow>();
        _pcPositions = new List<float>();
    }

    // Update is called once per frame
    void Update()
    {
        //if(Time.time > _lastPCMade + 1) {
        //if(Input.GetKeyDown(KeyCode.P)) {
        if(_willGeneratePC && _waveMarker.GetCurrentProgress() >= _nextPCposition && _previousMarkerPos < _nextPCposition) {
            CanMakePC = true;
            _willGeneratePC = false;
        }

        _previousMarkerPos = _waveMarker.GetCurrentProgress();
    }

    public void GeneratePC() {
        _willGeneratePC = true;
    }

    public void RegisterPC()
    {
        float thisPCPosition = _waveMarker.GetCurrentProgress();

        foreach (float pcPos in _pcPositions) {
            if(Mathf.Abs(pcPos - thisPCPosition) < 0.1f) {
                return; //have it already
            }
        }

        _pcPositions.Add(thisPCPosition);
        _pcPositions.Sort();

        UpdateNextPCPosition();
    }

    private void UpdateNextPCPosition()
    {
        //not entirely happy with this...seems to place dudes in weird spots sometimes.
        // SUPPOSED to ensure that guys are always spaced as far apart as possible.
        // (seems to work fine up until around 8 PCs)

        float maxDistBetween = 0;
        int index = -1;

        if(_pcPositions.Count == 1) {
            maxDistBetween = 1;
            index = 0;
        } else {
            for(int i = 0; i < _pcPositions.Count; ++i) {
                float dist = 0;
                if(i == _pcPositions.Count-1) {
                    dist = (_pcPositions[0] + 1) - _pcPositions[i];
                } else {
                    dist = _pcPositions[i + 1] - _pcPositions[i];
                }
                if(dist > maxDistBetween) {
                    maxDistBetween = dist;
                    index = i;
                }
            }
        }

        _nextPCposition = _pcPositions[index] + maxDistBetween/2;
        if(_nextPCposition > 1) {
            _nextPCposition -= 1;
        }

        /*
        string poses = "";
        foreach(float p in _pcPositions) {
            poses += p + " ";
        }

        Debug.Log(poses);
        Debug.Log("_nextPCposition: " +_nextPCposition);
        //*/
    }

    public int NumPCs()
    {
        if(_pcPositions.Count == 0) {
            return 1;
        }

        return _pcPositions.Count;
    }

    public float PosOfLastPC(float currentPos)
    {
        if(_pcPositions.Count == 0) {
            return 0;
        }

        for (int i = _pcPositions.Count - 1; i >= 0; --i) {
            if(_pcPositions[i] < currentPos) {
                return _pcPositions[i];
            }
        }

        return _pcPositions[_pcPositions.Count - 1];
    }

    public float PosOfNextPC(float currentPos)
    {
        if(_pcPositions.Count == 0) {
            return 0;
        }

        foreach (float pcPos in _pcPositions) {
            if(pcPos > currentPos) {
                return pcPos;
            }
        }

        return _pcPositions[0];
    }
}
